var express = require('express');
var router = express.Router();
const createError = require('http-errors');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = "mongodb://localhost:27017/annuaire";
const dbName = "annuaire";
const mongo = require('../bin/mongo');
const defaultAvatar = "https://iconscout.com/icon/avatar-376";


/* -------------------------------------------------------------------------- */
/*                                 CREATE contact                             */
/* -------------------------------------------------------------------------- */

router.post('/', (req, res) => {

  /* -------------------------- check datas received -------------------------- */

  if (req.body && req.body.nom && req.body.prenom && req.body.description && req.body.numero && req.body.mail)

    /* --------------------- check phone number not in DB -------------------- */

    mongo.getInstance().collection('contacts').findOne({ phone: req.body.phone }, (err, contact) => {
      if (err) throw err;
      if (user && user._id) {
        return next(createError(406));
      }

      /* ---------------------- create object user to insert ---------------------- */

      let newContact = {
        avatar: defaultAvatar,
        nom: req.body.nom,
        prenom : req.body.prenom,
        description: "",
        phone: "",
        mail: ""
      };

      /* ---------------------------- Insert user in DB --------------------------- */
      mongo.getInstance().collection('contacts').insertOne(newContact, (err, result) => {
        if (err) throw err;
        // verifier qu'il a bien été inséré
        if (result.insertedId) {
          // repond au client
          res.session.contact = newContact;
          res.session.contact._id = String(result.insertedId);
          return res.send('Contact created');
        }
        else
          return next(createError(402))
      });
    });
});
module.exports = router;