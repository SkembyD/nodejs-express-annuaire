var express = require('express');
var router = express.Router();
const createError = require('http-errors');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = "mongodb://localhost:27017/annuaire";
const dbName = "annuaire";
const mongo = require('../bin/mongo');
const defaultAvatar = "https://iconscout.com/icon/avatar-376";


/* -------------------------------------------------------------------------- */
/*                              GET contacts listing                          */
/* -------------------------------------------------------------------------- */

router.get('/', (req, res) => {
  mongo.getInstance().collection('contacts').find().toArray(function (err, results){

    console.log('results : ', results)
    console.log('req : ', req)
    if (err) {
      console.error(JSON.stringify(err));
    }
    res.render('index', {results})
  });
});

/* -------------------------------------------------------------------------- */
/*                              GET contacts details                          */
/* -------------------------------------------------------------------------- */

router.get('/:id', (req, res) => {

  mongo.getInstance().collection('contacts').findOne(
    { _id: ObjectId(req.params.id) },
    (err, result) => {
      if (err) throw err;
      res.render('contactDetails', result);
    });
});


module.exports = router;
