var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Annuaire Contact' });
});

router.get('/contactCreate', function(req, res, next){
  res.render('contactCreate',{ title: 'Annuaire Contact' });
});

router.get('/contactDetails', function(req, res, next){
  res.render('contactDetails', {title: 'Annuaire Contact'})
})
module.exports = router;
