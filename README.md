Application de gestion d'annuaire

afficher au démarrage une page d'accueil avec :

le nombre de contacts
les 3 derniers contacts enregistrés
la possibilité de créer un contact


une page listant tous les contacts par ordre alphabetique
lorsque l'on clic sur un coutact, on va sur la page qui lui est dédiée

Page du contact

visualiser la fiche du contact, notamment :

avatar
nom / prenom
description
numéros de téléphone (il peut en avoir plusieurs)
emails (il peut en avoir plusieurs)


modifier les informations liées à celui-ci (en AJAX)
supprimer le contact (en AJAX)